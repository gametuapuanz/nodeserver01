var express = require('express');
var app = express();

var server = app.listen(8081, function () {
    var port = server.address().port
    console.log("The port number of Server : %s", port)
});
app.get('/', function (req, res) {
    console.log("http://localhost:8081");
    console.log("Student Name");
    res.send('Rawich Loungsueb');
});

app.get('/1', function (req, res) {
    console.log("http://localhost:8081/1");
    console.log("Student Address");
    res.send('Bangsaen');
});
app.get('/2', function (req, res) {
    console.log("http://localhost:8081/2");
    console.log("Student Name + Address");
    res.send('Rawich Loungsueb Bangsaen');
});
app.get('/3', function (req, res) {
    console.log("http://localhost:8081/3");
    console.log("Student Name + Address + a good friend name");
    res.send('O-Live SaveHouse Tattoo a good friend name');
});